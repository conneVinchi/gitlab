## Oscar Peterson Boogie Blues Etude Pdf Free

 
  
 
**## Files you can download:
[Link 1](https://gohhs.com/2tF4XU)

[Link 2](https://miimms.com/2tF4XU)

[Link 3](https://urlcod.com/2tF4XU)

**

 
 
 
 
 
# How to Play Boogie Blues Etude by Oscar Peterson on Piano
 
If you are a fan of jazz piano, you might have heard of Oscar Peterson, one of the most influential and virtuosic pianists of the 20th century. He was known for his dazzling technique, swing feel, and musical expression. One of his famous compositions is Boogie Blues Etude, a fast and energetic piece that showcases his mastery of blues and boogie-woogie styles.
 
In this article, we will show you how to play Boogie Blues Etude by Oscar Peterson on piano, with some tips and tricks to help you learn it faster and easier. We will also provide you with a free PDF sheet music of the piece that you can download and print.
 
## What is Boogie Blues Etude?
 
Boogie Blues Etude is a solo piano piece composed by Oscar Peterson in 1974. It was recorded live at the Montreux Jazz Festival in Switzerland and released on his album The Trio. The piece is based on a 12-bar blues form in F major, but with some variations and modulations. It features a typical boogie-woogie bass line in the left hand, and a mix of blues scales, chords, arpeggios, and chromatic runs in the right hand. The tempo is very fast, around 240 beats per minute, and the piece lasts about 2 minutes and 40 seconds.
 
## How to Play Boogie Blues Etude?
 
To play Boogie Blues Etude by Oscar Peterson on piano, you need to have a good grasp of the blues and boogie-woogie styles, as well as a solid technique and rhythm. Here are some steps to help you learn the piece:
 
1. Listen to the original recording of the piece by Oscar Peterson. You can find it on YouTube or Spotify. Pay attention to how he plays the melody, harmony, rhythm, dynamics, articulation, and expression. Try to imitate his style and feel.
2. Download and print the free PDF sheet music of the piece from Musescore.com[^1^]. You can also use the online player to listen to the score and practice along with it. The score is a transcription of Oscar Peterson's live performance, so it may not be exactly accurate or consistent. However, it is a good reference for learning the main notes and chords of the piece.
3. Practice the piece slowly and gradually increase the speed. Start with a metronome at a comfortable tempo, such as 120 beats per minute. Practice each hand separately first, then together. Focus on accuracy, clarity, and coordination. Break down the piece into smaller sections and practice them repeatedly until you can play them smoothly.
4. Add some variations and improvisations to the piece. Oscar Peterson never played the same piece twice in exactly the same way. He always added some embellishments, substitutions, or improvisations to make his playing more interesting and personal. You can do the same by changing some notes, adding some grace notes or trills, or creating your own licks or solos over the blues form.
5. Have fun and enjoy playing Boogie Blues Etude by Oscar Peterson on piano. Remember that this piece is meant to be playful and expressive, not rigid or mechanical. Try to capture the spirit and energy of Oscar Peterson's playing and make it your own.

## Conclusion
 
Boogie Blues Etude by Oscar Peterson is a challenging but rewarding piece for jazz piano lovers. It requires a lot of skill and practice to play it well, but it also offers a lot of room for creativity and expression. By following the steps above, you can learn how to play Boogie Blues Etude by Oscar Peterson on piano faster and easier.
 
If you want to learn more about jazz piano or other genres of music, you can check out our online courses at Musescore.com[^1^]. We have 25 courses for piano learners of all levels and interests. You can also browse our huge collection of sheet music for piano and other instruments (over 1 million scores) and download or print them for free.
 
We hope you enjoyed this article and learned something new today. Happy playing!
 dde7e20689
 
 
