## Gandalf Discography (1981 - 2011)

 
  
 
**Gandalf Discography (1981 - 2011) ○ [https://gohhs.com/2tyO8J](https://gohhs.com/2tyO8J)**

 
 
 
 
 ``` 
# Gandalf: The Painter of Musical Landscapes
 
Gandalf is the artistic name of Heinz Strobl, an Austrian composer, musician and producer who has been creating instrumental electronic music with progressive elements since 1980. He describes himself as a "painter of musical landscapes" and his name originates from the good-hearted wizard in J.R.R. Tolkien's trilogy "Lord of the Rings".
 
Gandalf plays a great variety of instruments, such as acoustic and electric guitars, sitar, saz, charango, bouzuki, balaphon, piano, synthesizers and percussion. He blends acoustic, electronic and spherical sounds and weaves folk elements into symphonic structures to create his unique musical style. His work often reflects his love of nature and his commitment to preserving the environment. He also aims to create music that dissolves boundaries and celebrates the diversity and unity of cultures around the world.
 
Gandalf has released more than 50 albums in his career, spanning from 1980 to 2011. His discography includes solo albums as well as collaborations with other artists such as Steve Hackett, Tracy Hitchings and Galadriel. His music has been influenced by various genres and styles, such as progressive rock, new age, ambient, world music and classical music. Some of his most acclaimed albums are:
 
- Journey to an Imaginary Land (1980): His debut album, which established his progressive rock sound with influences from Mike Oldfield and Pink Floyd.
- Visions (1981): His second album, which featured more electronic and atmospheric elements and a concept based on dreams and visions.
- To Another Horizon (1983): His fourth album, which was a conceptual work inspired by the ancient cultures of Europe and Asia.
- Gallery of Dreams (1992): His 15th album, which was a collaboration with former Genesis guitarist Steve Hackett. The album combined Gandalf's symphonic soundscapes with Hackett's virtuosic guitar playing.
- Colours of the Earth (1994): His 17th album, which was a tribute to the beauty and diversity of nature. The album featured various ethnic instruments and vocals by Tracy Hitchings.
- Sanctuary (2009): His 46th album, which was a celebration of life and spirituality. The album featured guest appearances by singers Eva Novak and Christian Strobl.
- Erdenklang & Sternentanz (2011): His 50th album, which was a double CD with two different musical themes: earth sounds and star dances. The album was a culmination of his musical journey and a reflection of his personal philosophy.

Gandalf is one of the most prolific and influential artists in the instrumental electronic music scene. His music has been praised for its originality, creativity and emotional depth. He has also been recognized for his contribution to the development of progressive rock and new age music genres. He is a true master of musical landscapes who invites listeners to explore their own imagination and inner worlds.
 ``` dde7e20689
 
 
